package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readDataset() [][]int {
	file, err := os.ReadFile("dataset.txt")
	check(err)

	content := strings.Split(string(file), "\n")
	histEntries := make([][]int, 0)

	for _, line := range content {
		history := strings.Split(line, " ")
		histEntry := make([]int, 0)

		for _, entry := range history {
			digit, err := strconv.Atoi(entry)
			check(err)
			histEntry = append(histEntry, digit)
		}
		histEntries = append(histEntries, histEntry)
	}
	return histEntries
}

func getExtrapolatedNextValue(upperSlice []int) int {
	diffSlice := make([]int, 0)
	isZero := true
	extrapolate := 0

	for i := 0; i < len(upperSlice)-1; i++ {
		diff := upperSlice[i+1] - upperSlice[i]
		diffSlice = append(diffSlice, diff)
		if diff != 0 {
			isZero = false
		}
	}

	if isZero == false {
		extrapolate = getExtrapolatedNextValue(diffSlice)
	}

	extrapolate = extrapolate + upperSlice[len(upperSlice)-1]
	return extrapolate
}

func getExtrapolatedPreviousValue(upperSlice []int) int {
	diffSlice := make([]int, 0)
	isZero := true
	extrapolate := 0

	for i := 0; i < len(upperSlice)-1; i++ {
		diff := upperSlice[i+1] - upperSlice[i]
		diffSlice = append(diffSlice, diff)
		if diff != 0 {
			isZero = false
		}
	}

	if isZero == false {
		extrapolate = getExtrapolatedPreviousValue(diffSlice)
	}

	extrapolate = upperSlice[0] - extrapolate
	return extrapolate
}

func main() {
	histEntries := readDataset()
	extrapolatedNextValueSum := 0
	extrapolatedPreviousValueSum := 0

	for _, entry := range histEntries {
		extrapolatedValue := getExtrapolatedNextValue(entry)
		//fmt.Printf("%v -> Next Extrapolated: %v\n", entry, extrapolatedValue)
		extrapolatedNextValueSum += extrapolatedValue
	}

	fmt.Printf("Sum of next extrapolations: %v\n", extrapolatedNextValueSum)

	for _, entry := range histEntries {
		extrapolatedValue := getExtrapolatedPreviousValue(entry)
		//fmt.Printf("%v -> Previous Extrapolated: %v\n", entry, extrapolatedValue)
		extrapolatedPreviousValueSum += extrapolatedValue
	}

	fmt.Printf("Sum of previous extrapolations: %v\n", extrapolatedPreviousValueSum)
}
